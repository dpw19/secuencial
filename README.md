# Circuito secuencial

Este es un circuito secuencial simple para tiras LED Neon Flex,
basado en un contador de décadas CD4017, un timer LM555 y transistores 
bd139 como drivers para cada segmento.

Se desarrolló con KiCAD, puedes accedes a los archivos de diseño.

El diagrama esquemático es el siguiente
[Diagrama esquemático](img/01esquematico.png)


